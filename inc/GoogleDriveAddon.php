<?php namespace HM\BackUpWordPress;

class GoogleDriveAddon extends Addon {


	public function __construct( $plugin_version, $min_bwp_version, $service_class, $edd_download_file_name, $plugin_name, $plugin_settings_key, $plugin_settings_defaults ) {

		parent::__construct( $plugin_version, $min_bwp_version, $service_class, $edd_download_file_name, $plugin_name, $plugin_settings_key, $plugin_settings_defaults );

		add_action( 'admin_post_gdrive_unauth', array( $this, 'unauth' ) );
	}

	/**
	 * Clear authentication with Google Drive.
	 */
	public function unauth() {

		check_admin_referer( 'gdrive-unauth', 'gdrive-unauth-nonce' );

		if ( ! current_user_can( 'manage_options' ) ) {
			wp_die( 'Cheating are we?' );
		}

		$settings = $this->fetch_settings();

		$settings['refresh_token'] = '';
		$settings['access_token']  = '';

		update_site_option( 'hmbkpp_gdv_settings', $settings );

		wp_safe_redirect( wp_get_referer(), 303 );
		exit;

	}

	/**
	 * Fetch the settings from the database.
	 *
	 * @return mixed|void
	 */
	public function fetch_settings() {

		return get_site_option( $this->plugin_settings_key, $this->plugin_settings_defaults );

	}

}
