<?php namespace HM\BackUpWordPress;


/**
 * Class GoogleDriveBackUpService
 * @package HM\BackUpWordPress
 */
class GoogleDriveBackUpService extends Service {

	/**
	 * Human Friendly service name
	 *
	 * @var string
	 */
	public $name = 'Google Drive';

	/**
	 * The Google API client
	 *
	 * @var Google_Client
	 */
	protected $client;

	/**
	 * Google Drive Service
	 *
	 * @var Google_Service_Drive
	 */
	protected $service;

	/**
	 * Upload the backup to GDrive on the hmbkp_backup_complete
	 *
	 * @see  HM_Backup::do_action
	 *
	 * @param  string $action The action received from the backup
	 *
	 * @return void
	 */
	public function action( $action, Backup $backup ) {

		if ( 'hmbkp_backup_complete' === $action && $this->get_field_value( 'GDV' ) ) {

			$this->schedule->status->set_status( __( 'Uploading to Google Drive', 'backupwordpress' ) );

			$this->resumable_upload( $backup );

			$this->schedule->status->set_status( __( 'Cleaning up old backups', 'backupwordpress' ) );

			$this->delete_old_backups();

		}

	}

	/**
	 * Upload large files in chunks.
	 *
	 * @param Backup $backup
	 */
	protected function resumable_upload( Backup $backup ) {

		$this->auth( $backup );

		try {

			$backup_folder_id = $this->get_backup_folder();

			$filePath = $backup->get_backup_filepath();

			$filename = pathinfo( $filePath, PATHINFO_BASENAME );
			$file     = new \Google_Service_Drive_DriveFile();
			$file->setTitle( $filename );
			$file->setDescription( __( 'Backup of your website', 'backupwordpress' ) );

			// Set the parent folder

			$parent = new \Google_Service_Drive_ParentReference();
			$parent->setId( $backup_folder_id );
			$file->setParents( array( $parent ) );

			// File is smaller than 20Mb
			if ( ( 20000000 ) > filesize( $filePath ) ) {

				$result = $this->service->files->insert( $file, array(
					'data'       => file_get_contents( $filePath ),
					'mimeType'   => 'application/zip',
					'uploadType' => 'media',
				) );

			} else {

				$chunkSizeBytes = 1 * 1024 * 1024;

				// Call the API with the media upload, defer so it doesn't immediately return.
				$this->client->setDefer( true );

				$request = $this->service->files->insert( $file );

				$media = new \Google_Http_MediaFileUpload( $this->client, $request, 'application/zip', null, true, $chunkSizeBytes );

				$media->setFileSize( filesize( $filePath ) );

				$status = false;

				$handle = fopen( $filePath, 'rb' );

				while ( ! $status && ! feof( $handle ) ) {

					$chunk  = fread( $handle, $chunkSizeBytes );

					$status = $media->nextChunk( $chunk );

				}

				// The final value of $status will be the data from the API for the object
				// that has been uploaded.
				$result = false;
				if ( false !== $status ) {
					$result = $status;
				}

				fclose( $handle );

				// Reset to the client to execute requests immediately in the future.
				$this->client->setDefer( false );
			}
		} catch ( \Google_Service_Exception $e ) {

			$backup->warning( 'GDV', sprintf( __( 'An error occurred: %s', 'backupwordpress' ), $e->getCode() . ' ' . $e->getMessage() ) );

		} catch ( Exception $e ) {

			$backup->warning( 'GDV', sprintf( __( 'An error occurred: %s', 'backupwordpress' ), $e->getMessage() ) );

		}

	}

	/**
	 * Frees up space on the specified Google Drive acocunt by only keeping the number of
	 * backup files defined in the max backup settings and deleting older ones.
	 */
	protected function delete_old_backups() {

		$this->auth();

		$max_backups = absint( $this->get_field_value( 'gdv_max_backups' ) );

		$pattern = implode( '-', array(
				sanitize_title( str_ireplace( array( 'http://', 'https://', 'www' ), '', home_url() ) ),
				$this->schedule->get_id(),
				$this->schedule->get_type(),
			)
		);

		$backup_folder = $this->get_backup_folder();

		$file_list = $this->service->files->listFiles(
			array( 'q' => '\'' . $backup_folder . '\' in parents and ' . 'title contains \'' . $pattern . '\'' )
		);

		if ( ! ( $file_list instanceof \Google_Service_Drive_FileList ) ) {
			return;
		}

		$remote_files = $file_list->getItems();

		if ( ! empty( $remote_files ) ) {

			$file_names = array();

			// need to get ID and name
			foreach ( $remote_files as $file ) {
				$file_names[ $file['id'] ] = $file['originalFilename'];
			}

			// We need to remove old backups
			if ( count( $file_names ) > $max_backups ) {

				arsort( $file_names );

				$files_to_delete = array_slice( $file_names, $max_backups );

				// delete files
				foreach ( $files_to_delete as $key => $file ) {
					$this->service->files->delete( $key );
				}
			}
		}
	}

	/**
	 * Output the GDV form fields
	 */
	public function form() { ?>

		<table class="form-table">

			<tbody>

			<tr>

				<th scope="row">

					<label for="<?php echo $this->get_field_name( 'GDV' ); ?>"><?php _e( 'Send a copy of each backup to Google Drive', 'backupwordpress' ); ?></label>

				</th>

				<td>

					<input type="checkbox" <?php checked( $this->get_field_value( 'GDV' ) ) ?> id="<?php echo $this->get_field_name( 'GDV' ); ?>" name="<?php echo $this->get_field_name( 'GDV' ); ?>" value="1"/>

				</td>

			</tr>

			<tr>
				<th scope="row">

					<label for="<?php echo $this->get_field_name( 'gdrive_folder_id' ); ?>"><?php _e( 'Folder', 'backupwordpress' ); ?></label>
				</th>

				<td>
					<input type="text" id="<?php echo $this->get_field_name( 'gdrive_folder_id' ); ?>" name="<?php echo $this->get_field_name( 'gdrive_folder_id' ); ?>" value="<?php echo $this->get_field_value( 'gdrive_folder_id' ) ? $this->get_field_value( 'gdrive_folder_id' ) : 'BackUpWordPress'; ?>"/>

					<p class="description"><?php _e( 'The Google Drive folder name.', 'backupwordpress' ); ?></p>

				</td>

			</tr>

			<tr>

				<th scope="row">

					<label><?php _e( 'Google Drive Authentication', 'backupwordpress' ); ?></label>
				</th>

				<td>

					<?php
					$this->bootstrap();

					$authenticated = false;

					$settings = get_site_option( 'hmbkpp_gdv_settings' );

					if ( ! $settings ) {

						$authenticated = false;

					} elseif( $settings && empty( $settings['access_token'] ) ) {

						$authenticated = false;

					} else {

						$this->client->setAccessToken( $settings['access_token'] );

						if ( $this->client->isAccessTokenExpired() ) {

							$access_token = json_decode( $settings['access_token'] );

							$refresh_token = $access_token->refresh_token;

							$this->client->refreshToken( $refresh_token );
							$authenticated = true;

						} else {

							$authenticated = true;
						}

					}

					if ( ! $authenticated ) {
						$authUrl = $this->client->createAuthUrl();
					}
					?>

					<?php if ( ! $authenticated ) : ?>

						<a id="gdv-auth" target="_blank" href="<?php echo esc_url( $authUrl ); ?>" class="button-secondary button-right"><?php _e( 'Authenticate', 'backupwordpress' ); ?></a>

			<p class="description"><?php _e( 'Authenticate with Google Drive and paste the resulting code below', 'backupwordpress' ); ?></p>

			<tr>

				<th scope="row">
					<label for=""><?php _e( 'Authorization code', 'backupwordpress' ); ?></label>
				</th>

				<td>
					<input type="text" id="gdrive_auth_code" name="<?php echo $this->get_field_name( 'gdrive_auth_code' ); ?>" value=""/>

				</td>

			</tr>

			<?php else : ?>

						<a id="gdv-unauth" href="<?php echo esc_url( wp_nonce_url( add_query_arg( array( 'action' => 'gdrive_unauth' ), admin_url( 'admin-post.php' ) ), 'gdrive-unauth', 'gdrive-unauth-nonce' ) ); ?>" class="button-secondary button-right" data-hmbkpp-gdv-schedule-id="<?php esc_attr_e( $this->schedule->get_id() ); ?>"><?php _e( 'Clear authentication', 'backupwordpress' ); ?></a>

				</td>

			</tr>

			<?php endif; ?>

			<tr>

				<th scope="row">

					<label for="max_backups"><?php _e( 'Max backups', 'backupwordpress' ); ?></label>
				</th>

				<td>
					<?php

					$max_backups = $this->get_field_value( 'gdv_max_backups' );

					$max_backups = ( ! empty( $max_backups ) ) ? $max_backups : 3;

					?>

					<input type="number" min="1" step="1" id="max_backups"
					       name="<?php echo $this->get_field_name( 'gdv_max_backups' ); ?>"
					       value="<?php echo( empty( $max_backups ) ? 3 : $max_backups ); ?>"/>

					<p class="description"><?php _e( 'The maximum number of backups to store.', 'backupwordpress' ); ?></p>

				</td>

			</tr>

			<input type="hidden" name="is_destination_form" value="1"/>

			<?php if ( $authenticated ) : ?>

				<tr class="account-info">

					<th scope="row">
						<?php _e( 'Account info', 'backupwordpress' ); ?>
					</th>

					<td>

						<?php

						$service = new \Google_Service_Drive( $this->client );
						$about   = $service->about->get();
						$user    = $about->getUser();

						?>

						<p>
							<?php printf( __( 'Connected account email address: %s', 'backupwordpress' ), esc_html( $user->getEmailAddress() ) ); ?>

						</p>

					</td>

				</tr>

			<?php endif; ?>

			</tbody>

		</table>

	<?php
	}

	/**
	 * Output the GDV Constant help text
	 */
	public static function constant() {}

	/**
	 * Define as an empty function as we are using form
	 */
	public function field() {}

	/**
	 * Validate the data before saving, if there are errors, return them to the user
	 *
	 * @param  array $new_data the new data thats being saved
	 * @param  array $old_data the old data thats being overwritten
	 *
	 * @return array           any errors encountered
	 */
	public function update( &$new_data, $old_data ) {

		$errors = array();

		if ( isset( $new_data['gdv_max_backups'] ) ) {
			if ( empty( $new_data['gdv_max_backups'] ) || ! ctype_digit( $new_data['gdv_max_backups'] ) ) {
				$errors['gdv_max_backups'] = __( 'Max backups must be a number', 'backupwordpress' );
			}
		}

		if ( ! empty( $new_data['gdrive_auth_code'] ) ) {

			$ret = $this->auth( null, $new_data['gdrive_auth_code'] );
			if ( is_wp_error( $ret ) ) {
				$errors['GDV'] = $ret->get_error_message();
			}
		}

		return $errors;

	}

	/**
	 * The words to append to the main schedule sentence
	 *
	 * @return string The words that will be appended to the main schedule sentence
	 */
	public function display() {

		if ( $this->is_service_active() ) {
			return __( $this->name, 'backupwordpress' );
		}

	}

	/**
	 * Used to determine if the service is in use or not
	 *
	 * @return bool True if service is active
	 */
	public function is_service_active() {
		return (bool) $this->get_field_value( 'GDV' );
	}

	/**
	 * Initiate authentication
	 */
	public function bootstrap() {

		$this->client = new \Google_Client();

		// Get your credentials from the APIs Console
		$this->client->setClientId( '81190188225-cu57i0ninjdm78vsf22vavo5644dh35s.apps.googleusercontent.com' );
		$this->client->setClientSecret( 'h5NAXsR-21YKmak9uNWoaRLB' );
		$this->client->setRedirectUri( 'urn:ietf:wg:oauth:2.0:oob' );

		$this->client->setScopes( array( 'https://www.googleapis.com/auth/drive' ) );
		$this->client->setAccessType( 'offline' );
		$this->service = new \Google_Service_Drive( $this->client );

	}

	/**
	 * Get access token
	 *
	 * @param Backup $backup
	 * @param $auth_code
	 */
	protected function auth( $backup = null, $auth_code = null ) {

		$this->bootstrap();

		$settings = get_site_option( 'hmbkpp_gdv_settings' );

		if ( ! empty( $auth_code ) ) {

			try {
				// Exchange authorization code for access token
				$this->client->authenticate( $auth_code );

			} catch ( \Google_Exception $e ) {

				return new \WP_Error( 'hmbkp_auth_error', sprintf( __( 'An error occurred: %s', 'backupwordpress' ), $e->getCode() . ' ' . $e->getMessage() ) );

			}

		} else {
			if ( empty( $settings['access_token'] ) ) {
				return;

			} else {

				$this->client->setAccessToken( $settings['access_token'] );

				if ( $this->client->isAccessTokenExpired() ) {

					$access_token = json_decode( $settings['access_token'] );

					$refresh_token = $access_token->refresh_token;

					$this->client->refreshToken( $refresh_token );

				}

			}
		}

		$access_token = $this->client->getAccessToken();

		$settings['access_token'] = $access_token;

		$this->client->setAccessToken( $access_token );

		update_site_option( 'hmbkpp_gdv_settings', $settings );

	}

	/**
	 * @return array
	 */
	public static function intercom_data() { return array(); }

	/**
	 * Sends data to Intercom.
	 */
	public static function intercom_data_html() {}

	/**
	 * Determines the folder for storing backups
	 *
	 * @return string
	 */
	protected function get_backup_folder() {

		$backup_folder_id = '';

		if ( 0 < trim( strlen( $this->get_field_value( 'gdrive_folder_id' ) ) ) ) {

			$backup_folder = $this->get_field_value( 'gdrive_folder_id' );

		} else {

			$backup_folder = 'BackUpWordPress';

		}

		//get a list of folders
		$folder_list = $this->service->files->listFiles(
			array( 'q' => 'mimeType = \'application/vnd.google-apps.folder\'' )
		);


		foreach ( $folder_list as $folder ) {

			if ( $backup_folder === $folder['title'] ) {

				$backup_folder_id = $folder['id'];
			}
		}

		// folder not found so create
		if ( 0 === strlen( $backup_folder_id ) ) {

			$folder = new \Google_Service_Drive_DriveFile();

			$folder->setTitle( $backup_folder );

			$folder->setDescription( __( 'Backups of your website', 'backupwordpress' ) );

			$folder->setMimeType( 'application/vnd.google-apps.folder' );

			$result = $this->service->files->insert( $folder, array() );

			$backup_folder_id = $result['id'];
		}

		return $backup_folder_id;

	}

}

Services::register( __FILE__, 'HM\BackUpWordPress\GoogleDriveBackUpService' );
