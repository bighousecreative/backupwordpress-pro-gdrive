<?php

$container['addon_class'] = 'HM\\BackUpWordPress\\GoogleDriveAddon';
$container['checklicense_class'] = 'HM\\BackUpWordPress\\CheckLicense';
$container['addon_version'] = '2.1.14';
$container['min_bwp_version'] = '3.4.3';
$container['edd_download_file_name'] = 'BackUpWordPress To Google Drive';
$container['addon_settings_key'] = 'hmbkpp_gdv_settings';
$container['addon_settings_defaults'] = array( 'license_key' => '', 'license_status' => '', 'license_expired' => '', 'expiry_date' => '', 'access_token' => '' );
$container['service_class'] = 'GoogleDriveBackUpService';
$container['updater_class'] = 'HM\\BackUpWordPress\\PluginUpdater';
$container['prefix'] = 'gdv';
$container['plugin_name'] = 'gdrive';
