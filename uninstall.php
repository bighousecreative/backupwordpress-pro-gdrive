<?php

if ( ! current_user_can( 'activate_plugins' ) ) {
	exit;
}
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}

// delete plugin options
delete_site_option( 'hmbkpp_gdv_settings' );
delete_transient( 'hmbkp_license_data_gdv' );